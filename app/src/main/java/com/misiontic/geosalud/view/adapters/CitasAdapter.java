package com.misiontic.geosalud.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.ListaCitaEnfermeriaMVP;

import java.util.ArrayList;
import java.util.List;

public class CitasAdapter extends RecyclerView.Adapter<CitasAdapter.ViewHolder> {

    private List<ListaCitaEnfermeriaMVP.CitasListdto> data;

    public CitasAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<ListaCitaEnfermeriaMVP.CitasListdto> data){
        this.data = data;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_citas, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTv_UserName().setText(data.get(position).getClientName());
        holder.getTv_Date().setText(data.get(position).getCitaDate());
        holder.getTv_Service().setText(data.get(position).getSercicio());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private  TextView tv_UserName;
        private  TextView tv_Date;
        private  TextView tv_Service;

        public ViewHolder(View view){
            super(view);

            InitUI(view);
        }

        private void InitUI(View view) {
            tv_UserName = view.findViewById(R.id.name_Profesional);
            tv_Date = view.findViewById(R.id.fecha_Hora);
            tv_Service = view.findViewById(R.id.service_type);
        }

        public TextView getTv_UserName() {
            return tv_UserName;
        }

        public TextView getTv_Date() {
            return tv_Date;
        }

        public TextView getTv_Service() {
            return tv_Service;
        }






    }
}

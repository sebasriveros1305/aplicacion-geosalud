package com.misiontic.geosalud.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.SuroccidenteMVP;

import java.util.ArrayList;
import java.util.List;

public class SuroccidenteAdapter extends RecyclerView.Adapter<SuroccidenteAdapter.ViewHolder> {

    private List<SuroccidenteMVP.SuroccidenteDto> data;
    private OnItemClickListener listener;

    public SuroccidenteAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<SuroccidenteMVP.SuroccidenteDto> data){
        this.data = data;
        //this.notifyDataSetChanged();

    }
    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hospital, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SuroccidenteMVP.SuroccidenteDto item = data.get(position);

        if (listener != null) {
            holder.itemView.setOnClickListener(v -> {
                listener.onItemClicked(item);
            });
        }
    holder.getIvName().setText(data.get(position).getHospital());
    holder.getIvAddress().setText(data.get(position).getAddress());
    holder.getIvTelephone().setText(data.get(position).getTelephone());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private  ImageView ivHospital;
        private  TextView ivName;
        private  TextView ivAddress;
        private  TextView ivTelephone;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            initUI(view);
        }

        private void initUI(View view) {
            ivHospital = view.findViewById(R.id.iv_hospital);
            ivName = view.findViewById(R.id.iv_name);
            ivAddress = view.findViewById(R.id.iv_address);
            ivTelephone = view.findViewById(R.id.iv_telephone);

        }

        public ImageView getIvHospital() {
            return ivHospital;
        }

        public TextView getIvName() {
            return ivName;
        }

        public TextView getIvAddress() {
            return ivAddress;
        }

        public TextView getIvTelephone() {
            return ivTelephone;
        }
    }

    public static interface OnItemClickListener {
        void onItemClicked(SuroccidenteMVP.SuroccidenteDto item);
    }
}

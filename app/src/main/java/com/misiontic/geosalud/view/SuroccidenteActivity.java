package com.misiontic.geosalud.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.SuroccidenteMVP;
import com.misiontic.geosalud.presenter.SuroccidentePresenter;
import com.misiontic.geosalud.view.adapters.SuroccidenteAdapter;

import java.util.ArrayList;
import java.util.List;

public class SuroccidenteActivity extends AppCompatActivity implements SuroccidenteMVP.View {

    private DrawerLayout drawerLayout;


    private RecyclerView rvHospital;
    private FloatingActionButton btnNewSale;

    private SuroccidenteMVP.Presenter presenter;
    private SuroccidenteAdapter suroccidenteAdapter;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suroccidente);

        presenter = new SuroccidentePresenter( SuroccidenteActivity.this);

        initUI();

        loadData();
    }

    private void loadData() {
        presenter.loadSuroccidente();

    }

    private void initUI() {
drawerLayout = findViewById(R.id.drawer_layout);

rvHospital = findViewById(R.id.rv_hospital);
suroccidenteAdapter = new SuroccidenteAdapter();
suroccidenteAdapter.setListener(item -> presenter.onSelectItem(item));
rvHospital.setAdapter(suroccidenteAdapter);
rvHospital.setLayoutManager(new LinearLayoutManager(SuroccidenteActivity.this));

    }


    @Override
    public Activity getActivity() {
        return SuroccidenteActivity.this;
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void showSuroccidente(List<SuroccidenteMVP.SuroccidenteDto> hospital) {
        suroccidenteAdapter.setData(hospital);

    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(SuroccidenteActivity.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);

    }

}
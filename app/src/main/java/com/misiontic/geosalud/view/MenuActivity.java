package com.misiontic.geosalud.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.misiontic.geosalud.R;

public class MenuActivity extends AppCompatActivity {

    private AppCompatButton button2;
    private  AppCompatButton btn_farmacia;
    private AppCompatButton btn_atencioncercana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        InitUI();
    }

    @Override
    public void onBackPressed() {
        SharedPreferences preferences = getSharedPreferences("auth", Context.MODE_PRIVATE);
        preferences.edit()
                .putBoolean("isLogin",false)
                .apply();

        super.onBackPressed();
    }

    private void InitUI() {

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(v -> onClickBtnEnfermeria());

        btn_farmacia = findViewById(R.id.button5);
        btn_farmacia.setOnClickListener(v ->  onClickBtnFarmacia());

        btn_atencioncercana = findViewById(R.id.button3);
        btn_atencioncercana.setOnClickListener(v -> onClickBtnatencioncercana());

    }

    private void onClickBtnEnfermeria() {
        Intent intent = new Intent(MenuActivity.this, MenuEnfermeriaActivity.class);
        startActivity(intent);
    }

    private void onClickBtnFarmacia(){
        Intent intent = new Intent( MenuActivity.this , FarmaciaActivity.class);
        startActivity(intent);
    }
    private void onClickBtnatencioncercana(){
        Intent intent = new Intent( MenuActivity.this , AtencioncercanaActivity.class);
        startActivity(intent);
    }
}
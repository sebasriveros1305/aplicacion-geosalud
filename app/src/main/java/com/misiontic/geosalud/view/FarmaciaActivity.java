package com.misiontic.geosalud.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.FarmaciaMVP;
import com.misiontic.geosalud.presenter.FarmaciaPresenter;
import com.misiontic.geosalud.view.adapters.FarmaciaAdapter;

import java.util.List;

public class FarmaciaActivity extends AppCompatActivity implements FarmaciaMVP.View {
    private FloatingActionButton btnCancelar;

    private LinearProgressIndicator pbWait;
    private RecyclerView rvFarmacia;
    private FarmaciaMVP.Presenter presenter;
    private FarmaciaAdapter farmaciaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmacia);

        presenter = new FarmaciaPresenter(FarmaciaActivity.this);
        initUI();
        presenter.LoadFarmacia();

    }

    private void initUI() {
        btnCancelar = findViewById(R.id.btn_cancelar);
        btnCancelar.setOnClickListener(v -> presenter.onClikCancel());

        pbWait = findViewById(R.id.pb_wait);

        rvFarmacia = findViewById(R.id.rv_farmacia);
        rvFarmacia.setLayoutManager(new LinearLayoutManager(FarmaciaActivity.this));
        farmaciaAdapter = new FarmaciaAdapter();
        farmaciaAdapter.setClickFarmacia(new FarmaciaAdapter.OnItemClickFarmacia() {
            @Override
            public void OnItemClikc(FarmaciaMVP.FarmaciaDto item) {
                presenter.OnSelectItem(item);
            }
        });
        rvFarmacia.setAdapter(farmaciaAdapter);

    }



    @Override
    public Activity getAtivity() {
        return FarmaciaActivity.this;
    }

    @Override
    public void showProgresssBar() {
        pbWait.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);

    }

    @Override
    public void showFarmacia(List<FarmaciaMVP.FarmaciaDto> farmacia) {
        farmaciaAdapter.setData(farmacia);
    }

    @Override
    public void showManuActivity() {
        Intent inten = new Intent(FarmaciaActivity.this, MenuActivity.class);
        startActivity(inten);
    }

    @Override
    public void openDetailActivity(Bundle params) {
        //Intent  intent = new Intent(FarmaciaActivity.this, MenuActivity.class);
        //intent.putExtra(params);
        Toast.makeText(FarmaciaActivity.this, params.getString("name"),Toast.LENGTH_SHORT).show();
    }
}
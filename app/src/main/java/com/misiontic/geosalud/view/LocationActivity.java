package com.misiontic.geosalud.view;

import androidx.fragment.app.FragmentActivity;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.misiontic.geosalud.R;

import java.util.List;
import java.util.Locale;


public class LocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);

        String hospital = getIntent().getStringExtra("hospital");
        String address = getIntent().getStringExtra("address");
        String telephone = getIntent().getStringExtra("telephone");

        LatLng location = new LatLng(4,-72);
        try {
            Geocoder geo = new Geocoder(LocationActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocationName(hospital, 1);
            if (!addresses.isEmpty()) {
                location = new LatLng(addresses.get(0).getLatitude(),addresses.get(0).getLongitude());

            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }
        mMap.addMarker(new MarkerOptions().position(location).title(hospital + " - " + address + "_" + telephone));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 14f));
    }
}
package com.misiontic.geosalud.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.misiontic.geosalud.R;


public class AtencioncercanaActivity extends AppCompatActivity {


    private AppCompatButton btn_suroccidente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atencioncercana);


        initUI();
    }

    private void initUI() {

        btn_suroccidente = findViewById(R.id.suroccidente);
        btn_suroccidente.setOnClickListener(v->onClickBtnsuroccidente());

    }

    private void onClickBtnsuroccidente() {
        Intent intent = new Intent(AtencioncercanaActivity.this, SuroccidenteActivity.class);
        startActivity(intent);
    }
    }
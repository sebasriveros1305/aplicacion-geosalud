package com.misiontic.geosalud.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.FarmaciaMVP;

import java.util.ArrayList;
import java.util.List;

public class FarmaciaAdapter extends RecyclerView.Adapter<FarmaciaAdapter.ViewHolder>{

    private List<FarmaciaMVP.FarmaciaDto> data;

    private OnItemClickFarmacia clickFarmacia;

    public FarmaciaAdapter() {
        this.data = new ArrayList<>();
    }

    public  void setData(List<FarmaciaMVP.FarmaciaDto> data){
        this.data=data;
    }

    public  void setClickFarmacia(OnItemClickFarmacia clickFarmacia){
        this.clickFarmacia = clickFarmacia;
    }

    @NonNull
    @Override
    public FarmaciaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_farmacia, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FarmaciaAdapter.ViewHolder holder, int position) {

        FarmaciaMVP.FarmaciaDto  item = data.get(position);
        if(clickFarmacia!=null) {
            holder.itemView.setOnClickListener(v -> {
                clickFarmacia.OnItemClikc(item);

            });
        }
        holder.getTvname().setText(item.getName());
        holder.getTvaddress().setText(item.getAddress());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private  TextView tvname;
        private  TextView tvaddress;


        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            initUi(view);

        }

        private void initUi(View view) {
            tvname=view.findViewById(R.id.iv_name);
            tvaddress = view.findViewById(R.id.iv_address);


        }

        public TextView getTvname() {
            return tvname;
        }

        public TextView getTvaddress() {
            return tvaddress;
        }


    }

    public static interface OnItemClickFarmacia{
        void OnItemClikc(FarmaciaMVP.FarmaciaDto  item);
    }
}
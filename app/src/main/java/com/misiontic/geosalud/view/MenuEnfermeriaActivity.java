package com.misiontic.geosalud.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.MenuEnfermeriaMVP;
import com.misiontic.geosalud.presenter.MenuEnfermeriaPresenter;

public class MenuEnfermeriaActivity extends AppCompatActivity implements MenuEnfermeriaMVP.View {

    private AppCompatButton bntAtencion;
    private AppCompatButton btnAcompanamiento;
    private AppCompatButton btnConsultaCitas;

    private MenuEnfermeriaMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_enfermeria);

        presenter = new MenuEnfermeriaPresenter(MenuEnfermeriaActivity.this);

        InitUI();

    }

    private void InitUI() {
        bntAtencion=findViewById(R.id.btn_atencion);
        bntAtencion.setOnClickListener(v -> presenter.onAtencionCasaClick() );

        btnAcompanamiento=findViewById(R.id.btn_acompanamiento);
        btnAcompanamiento.setOnClickListener(v -> presenter.onAcompanamimentoClick() );

        btnConsultaCitas = findViewById(R.id.btn_consultaCitas);
        btnConsultaCitas.setOnClickListener(v -> presenter.onConsultaCitaClick());

    }

    @Override
    public void showAgendaTuCita(){
        Intent intent= new Intent(MenuEnfermeriaActivity.this, CitaEnfermeriaActivity.class);
        startActivity(intent);
    }

    @Override
    public void showListaCitasEnfermeria() {
        Intent intent= new Intent(MenuEnfermeriaActivity.this, ListaCitasEnfermeriaActivity.class);
        startActivity(intent);

    }

}
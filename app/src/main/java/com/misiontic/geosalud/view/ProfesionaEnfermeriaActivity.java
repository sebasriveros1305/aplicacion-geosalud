package com.misiontic.geosalud.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.ProfesionalEnfermeriaMVP;
import com.misiontic.geosalud.presenter.ProfesionalEnfermeriaPresenter;
import com.misiontic.geosalud.view.adapters.ProfesionalAdapter;

import java.util.List;

public class ProfesionaEnfermeriaActivity extends AppCompatActivity implements ProfesionalEnfermeriaMVP.View{

    private RecyclerView rvListProfesionales;
    private LinearProgressIndicator pb_wait;

    private ProfesionalEnfermeriaMVP.presenter presenter;
    private ProfesionalAdapter profesionalAdapter;

    private FloatingActionButton btnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesiona_enfermeria);

        presenter = new ProfesionalEnfermeriaPresenter(ProfesionaEnfermeriaActivity.this);

        InitUI();
        presenter.loadProfesionales();

    }

    private void InitUI() {
        btnSave=findViewById(R.id.btn_save);
        btnSave.setOnClickListener(v -> onClickSave() );

        rvListProfesionales =  findViewById(R.id.rvList_Profesionales);
        rvListProfesionales.setLayoutManager(new LinearLayoutManager(ProfesionaEnfermeriaActivity.this));
        profesionalAdapter = new ProfesionalAdapter();
        rvListProfesionales.setAdapter(profesionalAdapter);

        pb_wait = findViewById(R.id.pb_wait);

    }


    private void onClickSave() {
        Intent intent = new Intent(ProfesionaEnfermeriaActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() { return ProfesionaEnfermeriaActivity.this;}

    @Override
    public void showProgressBar() { pb_wait.setVisibility(View.VISIBLE); }

    @Override
    public void hideProgressBar() { pb_wait.setVisibility(View.GONE); }

    @Override
    public void showProfesionales(List<ProfesionalEnfermeriaMVP.ProfesionalesListdto> listProfesionales) {
        profesionalAdapter.setData(listProfesionales);

    }
}
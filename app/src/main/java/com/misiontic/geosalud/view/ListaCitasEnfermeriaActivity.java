package com.misiontic.geosalud.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.ListaCitaEnfermeriaMVP;
import com.misiontic.geosalud.presenter.ListaCitasEnfermeriaPresenter;
import com.misiontic.geosalud.view.adapters.CitasAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListaCitasEnfermeriaActivity extends AppCompatActivity implements ListaCitaEnfermeriaMVP.View {

    private RecyclerView rvListCitas;

    private LinearProgressIndicator pbWait;

    private ListaCitaEnfermeriaMVP.Presenter presenter;
    private CitasAdapter citasAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_citas_enfermeria);

        presenter= new ListaCitasEnfermeriaPresenter(ListaCitasEnfermeriaActivity.this);

        InitUI();
        presenter.loadCitas();
    }

    private void InitUI() {

        rvListCitas = findViewById(R.id.rvList_Citas);
        rvListCitas.setLayoutManager(new LinearLayoutManager(ListaCitasEnfermeriaActivity.this));
        citasAdapter= new CitasAdapter();
        rvListCitas.setAdapter(citasAdapter);

        pbWait=findViewById(R.id.pb_wait);
    }

    @Override
    public Activity getActivity() {
        return ListaCitasEnfermeriaActivity.this;
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {

        pbWait.setVisibility(View.GONE);
    }

    @Override
    public void showCitas(List<ListaCitaEnfermeriaMVP.CitasListdto> citas) {
        citasAdapter.setData(citas);
    }

}
package com.misiontic.geosalud.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.CitaEnfermeriaMVP;
import com.misiontic.geosalud.presenter.CitaEnfermeriaPresenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class CitaEnfermeriaActivity extends AppCompatActivity implements CitaEnfermeriaMVP.View {

    private TextInputLayout tilCalendar;
    private TextInputEditText etCalendar;
    private Date selectedDate;
    private AppCompatButton btnSave;

    private CitaEnfermeriaMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citaenfermeria);
       // Log.e("msj","inicia sesion")

        presenter= new CitaEnfermeriaPresenter(CitaEnfermeriaActivity.this);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(v -> presenter.onSaveClick());
        
       InitParameter();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle savedInstanceState) {

        savedInstanceState.putString("calendar", etCalendar.getText().toString());

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        etCalendar.setText(savedInstanceState.getString("calendar"));

        try {
            selectedDate= SimpleDateFormat.getDateInstance().parse(savedInstanceState.getString("calendar"));
        } catch (ParseException e) {
            selectedDate=null;
        }
    }

    private void InitParameter() {
        tilCalendar= findViewById(R.id.til_calendar);
        tilCalendar.setEndIconOnClickListener(v -> onCalendarClick());
        etCalendar = findViewById(R.id.et_calendar);
    }

    @Override
    public CitaEnfermeriaMVP.CalendarInfo getCalendarInfo() {
        return new CitaEnfermeriaMVP.CalendarInfo(etCalendar.getText().toString().trim());
    }

    @Override
    public void showCalendarError(String error) {
        tilCalendar.setBoxBackgroundColor(Color.RED);
        //Toast.makeText(CitaEnfermeriaActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProfesionalSalud() {
        tilCalendar.setBoxBackgroundColor(Color.LTGRAY);
        Intent intent = new Intent(CitaEnfermeriaActivity.this, ProfesionaEnfermeriaActivity.class);
        startActivity(intent);

    }

    private void onCalendarClick() {
        long today = MaterialDatePicker.todayInUtcMilliseconds();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(today);
        calendar.add(Calendar.DAY_OF_MONTH,0);

        CalendarConstraints constraints = new CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.from(calendar.getTimeInMillis()))
                .build();

        if(selectedDate != null){
            calendar.setTime(selectedDate);
        }

        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(R.string.txt_seleccione_fecha)
                .setSelection(calendar.getTimeInMillis())
                .setCalendarConstraints(constraints)
                .build();
        datePicker.addOnPositiveButtonClickListener(this::setSelectedDate);

        datePicker.show(getSupportFragmentManager(),"date");
        tilCalendar.setBoxBackgroundColor(Color.LTGRAY);


    }

    private void setSelectedDate(Long selection) {
        selectedDate = new Date(selection);
        etCalendar.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }



}
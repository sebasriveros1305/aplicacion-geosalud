package com.misiontic.geosalud.view;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.RegisterMVP;
import com.misiontic.geosalud.presenter.RegisterPresenter;

public class RegistroActivity extends AppCompatActivity implements RegisterMVP.View {

    private AppCompatButton btnRegister;
    private TextInputLayout tilname;
    private TextInputEditText etname;
    private TextInputLayout tillastname;
    private TextInputEditText etlastname;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilcel;
    private TextInputEditText etcel;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;
    private TextInputLayout tilconPassword;
    private TextInputEditText etconPassword;


    private RegisterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        presenter = new RegisterPresenter(RegistroActivity.this);
        initUI();


    }

    private void initUI() {

        btnRegister= findViewById(R.id.btn_Register);
        btnRegister.setOnClickListener(v -> presenter.onRegisterclick());

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tillastname = findViewById(R.id.til_lastname);
        etlastname = findViewById(R.id.et_lastname);

        tilname = findViewById(R.id.til_name);
        etname = findViewById(R.id.et_name);

        tilcel = findViewById(R.id.til_cel);
        etcel = findViewById(R.id.et_cel);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        tilconPassword = findViewById(R.id.til_conpassword);
        etconPassword = findViewById(R.id.et_conpassword);


    }


    @Override
    public Activity getActivity() {
        return RegistroActivity.this;
    }

    @Override
    public RegisterMVP.RegisterInfo getRegisterInfo() {
        return new RegisterMVP.RegisterInfo(etname.getText().toString().trim(),
                etlastname.getText().toString().trim(),
                etEmail.getText().toString().trim(),
                etcel.getText().toString().trim(),
                etPassword.getText().toString().trim(),
                etconPassword.getText().toString().trim());
    }

    @Override
    public void showNameError(String Error) {

        tilname.setError(Error);
    }

    @Override
    public void showLastnameError(String Error) {
        tillastname.setError(Error);

    }

    @Override
    public void showEmailError(String Error) {
        tilEmail.setError(Error);
    }

    @Override
    public void showCelError(String Eror) {
        tilcel.setError(Eror);
    }

    @Override
    public void showPasswordError(String Error) {
        tilPassword.setError(Error);
    }

    @Override
    public void showConPasswordError(String Error) {
        tilconPassword.setError(Error);
    }

    @Override
    public void showMenuActivity() {
        Intent intent = new Intent(RegistroActivity.this,MenuActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String Error) {
        Toast.makeText(RegistroActivity.this, Error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPasswordGeneralError(String Error) {
        Toast.makeText(RegistroActivity.this, Error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        btnRegister.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        btnRegister.setEnabled(true);
    }


}
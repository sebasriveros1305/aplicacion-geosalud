package com.misiontic.geosalud.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.ProfesionalEnfermeriaMVP;

import java.util.ArrayList;
import java.util.List;

public class ProfesionalAdapter extends RecyclerView.Adapter<ProfesionalAdapter.ViewHolder> {

    private List<ProfesionalEnfermeriaMVP.ProfesionalesListdto> data;

    public ProfesionalAdapter(){this.data= new ArrayList<>();}

    public void setData(List<ProfesionalEnfermeriaMVP.ProfesionalesListdto> data){
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_profesionales, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTv_ProfesionalName().setText(data.get(position).getProfesionalName());
        holder.getTv_Date().setText(data.get(position).getCitaDate());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private  TextView tv_ProfesionalName;
        private  TextView tv_Date;

        public ViewHolder(View view){
            super(view);

            InitUI(view);
        }

        private void InitUI(View view) {
            tv_ProfesionalName = view.findViewById(R.id.nombre_profesional);
            tv_Date = view.findViewById(R.id.cita_profesional);
        }

        public TextView getTv_ProfesionalName() {
            return tv_ProfesionalName;
        }

        public TextView getTv_Date() {
            return tv_Date;
        }
    }


}

package com.misiontic.geosalud.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misiontic.geosalud.R;
import com.misiontic.geosalud.mvp.LoginMVP;
import com.misiontic.geosalud.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private CircularProgressIndicator pbWait;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;
    private AppCompatButton btnLogin;
    private AppCompatButton btnFacebook;
    private AppCompatButton btnTwitter;
    private AppCompatButton btnInscribirse;
    private AppCompatButton btnOlvidosucontrasena;


    private LoginMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(LoginActivity.this);
        presenter.isLoggedUser();
        initUI();
    }


    private void initUI() {

        pbWait = findViewById(R.id.pb_wait);
        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);


        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> presenter.onLoginClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(v -> presenter.onFacebookClick());

        btnTwitter = findViewById(R.id.btn_twitter);
        btnTwitter.setOnClickListener(v -> presenter.onTwitterClick());

        btnInscribirse= findViewById(R.id.btn_inscribirse);
        btnInscribirse.setOnClickListener(v -> presenter.onInscribirseClick());

        AppCompatButton btnInscribirse = findViewById(R.id.btn_inscribirse);
        btnInscribirse.setOnClickListener(v -> presenter.onInscribirseClick());

        btnOlvidosucontrasena = findViewById(R.id.btn_olvidó_su_contraseña);
        btnOlvidosucontrasena.setOnClickListener(v -> presenter.onOlvidosucontrasenaClick());


    }


    @Override
    public Activity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showMenuActivity() {
        Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    @Override
    public void ShowgenerarError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgresBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnFacebook.setEnabled(false);
        btnTwitter.setEnabled(false);
        btnOlvidosucontrasena.setEnabled(false);
        btnInscribirse.setEnabled(false);
    }

    @Override
    public void hideProgresBar() {
        pbWait.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnFacebook.setEnabled(true);
        btnTwitter.setEnabled(true);
        btnOlvidosucontrasena.setEnabled(true);
        btnInscribirse.setEnabled(true);
    }

    @Override
    public void showRegister() {
        Intent intent= new Intent(LoginActivity.this, RegistroActivity.class);
        startActivity(intent);
    }
}

package com.misiontic.geosalud.mvp;

import android.app.Activity;


import java.util.List;

public interface LoginMVP {

    interface Model {

        void validateCredentials(String email, String password,
                                 ValidateCredentialsCallback callback);

        void getAllUsers(getUserCallBack<List<UserInfo>> callBack);

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }
        interface getUserCallBack<T> {
            void onSuccess(T data);

            void onFailure();
        }
    }
    interface Presenter {

        void onLoginClick ();

        void onFacebookClick ();

        void onTwitterClick ();

        void onInscribirseClick ();

        void onOlvidosucontrasenaClick();

        void isLoggedUser();
    }

    interface View {

        Activity getActivity();

        LoginInfo getLoginInfo ();

        void showEmailError(String error);

        void showPasswordError(String error);

        void showMenuActivity();

        void ShowgenerarError(String error);

        void showProgresBar();

        void hideProgresBar();

        void showRegister();
    }

    class LoginInfo {

        private String email;
        private String password;

        public LoginInfo (String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail (){
            return email;
        }
        public String getPassword (){
            return password;
        }
    }

    class UserInfo{
        private String email;
        private String password;
        private String name;
        private String telefono;

        public UserInfo(String email, String password, String name, String telefono) {
            this.email = email;
            this.password = password;
            this.name = name;
            this.telefono = telefono;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public String getName() {
            return name;
        }

        public String getTelefono() {
            return telefono;
        }

        @Override
        public String toString() {
            return "UserInfo{" +
                    "email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", name='" + name + '\'' +
                    ", telefono='" + telefono + '\'' +
                    '}';
        }
    }

}
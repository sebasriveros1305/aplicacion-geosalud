package com.misiontic.geosalud.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

public interface SuroccidenteMVP {

    interface Model {
        void loadSuroccidente(loadSuroccidenteCallback callback);

        interface loadSuroccidenteCallback{
            void setSuroccidente(List<SuroccidenteDto>hospital);
        }
    }

    interface Presenter {
        void loadSuroccidente();

        void onSuroccidenteClick();

        void onSelectItem(SuroccidenteDto item);

        void onNewHospitalClick();


    }

    interface View {

        Activity getActivity ();

        void showProgressBar();

        void hideProgressBar();

        void showSuroccidente(List<SuroccidenteDto> hospital);

        void openLocationActivity(Bundle params);
    }

    class SuroccidenteDto{
        private String hospital;
        private String address;
        private String telephone;

        public SuroccidenteDto(String hospital, String address, String telephone) {
            this.hospital = hospital;
            this.address = address;
            this.telephone = telephone;
        }

        public String getHospital() {
            return hospital;
        }

        public String getAddress() {
            return address;
        }

        public String getTelephone() {
            return telephone;
        }
    }
}

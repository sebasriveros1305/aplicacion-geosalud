package com.misiontic.geosalud.mvp;

import android.app.Activity;

public interface AtencioncercanaMVP {

    interface Model{

    }

    interface Presenter {
        void loadAtencioncercana();

        void onNewRegionClick();


    }

    interface View {

        Activity getActivity ();

        void showProgressBar();

        void hideProgressBar();

    }
}

package com.misiontic.geosalud.mvp;

import java.util.Date;

public interface CitaEnfermeriaMVP {

    interface Model{

    }

    interface Presenter{
        void onSaveClick();

    }

    interface View{
        CalendarInfo getCalendarInfo();

        void showCalendarError(String error);

        void showProfesionalSalud();
    }

    class CalendarInfo{
        private String calendarDate;

        public CalendarInfo(String calendarDate) {
            this.calendarDate = calendarDate;
        }

        public String getCalendarDate() {
            return calendarDate;
        }
    }
}

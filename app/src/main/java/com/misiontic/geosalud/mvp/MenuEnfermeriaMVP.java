package com.misiontic.geosalud.mvp;

public interface MenuEnfermeriaMVP {

    interface Model{

    }

    interface Presenter{
        void onAtencionCasaClick();
        void onAcompanamimentoClick();
        void onConsultaCitaClick();

    }

    interface View{
        void showAgendaTuCita();

        void showListaCitasEnfermeria();
    }
}
package com.misiontic.geosalud.mvp;

import android.app.Activity;

import java.util.List;

public interface ProfesionalEnfermeriaMVP {

    interface Model{
        void loadProfesionales(ProfesionalEnfermeriaMVP.Model.loadProfesionalesCallBack callBack);

        interface  loadProfesionalesCallBack{
            void setProfesionales(List<ProfesionalEnfermeriaMVP.ProfesionalesListdto> profesionales);

        }

    }

    interface presenter{
        void loadProfesionales();

    }

    interface View{
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showProfesionales(List<ProfesionalEnfermeriaMVP.ProfesionalesListdto> listProfesionales);

    }

    class ProfesionalesListdto{

        private String ProfesionalName;
        private String CitaDate;


        public ProfesionalesListdto(String profesionalName, String citaDate) {
            this.ProfesionalName = profesionalName;
            this.CitaDate = citaDate;
        }

        public String getProfesionalName() {
            return ProfesionalName;
        }

        public String getCitaDate() {
            return CitaDate;
        }
    }
}
package com.misiontic.geosalud.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

public interface FarmaciaMVP {

    interface Model{

        void loadFarmacia(LoadFarmaciaCallback callback);
        interface LoadFarmaciaCallback{

            void setFarmacia(List<FarmaciaDto> Farmacia);
        }
    }

    interface Presenter{
        void LoadFarmacia();


        void onClikCancel();

        void OnSelectItem(FarmaciaDto item);
    }

    interface View{
        Activity getAtivity();

        void showProgresssBar();

        void hideProgressBar();

        void showFarmacia(List<FarmaciaDto> farmacia);

        void showManuActivity();

        void openDetailActivity(Bundle params);
    }
    class  FarmaciaDto{

        private String name;
        private String address;


        public FarmaciaDto(String name, String address) {
            this.name = name;
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public String getAddress() {
            return address;
        }


    }



}

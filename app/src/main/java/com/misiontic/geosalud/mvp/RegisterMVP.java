package com.misiontic.geosalud.mvp;

import android.app.Activity;

import com.misiontic.geosalud.model.database.entity.User;

public interface RegisterMVP {

    interface  Model {

        void ValidateCredentials(User user,
                                 ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback{
            void onSuccess();

            void onFailure();
        }
    }

    interface Presenter {

        void onRegisterclick();


    }

    interface View {
        Activity getActivity();

        RegisterInfo getRegisterInfo();

        void showNameError(String Error);

        void showLastnameError(String Error);

        void showEmailError(String Error);

        void showCelError(String Eror);

        void showPasswordError(String Error);

        void showConPasswordError(String Error);

        void showMenuActivity();
        void showGeneralError(String Error);

        void showPasswordGeneralError(String Error);

        void showProgress();

        void hideProgress();
    }

    class RegisterInfo{

        private String name;
        private String last_name;
        private String email;
        private String cel;
        private String password;
        private String conpassword;

        public RegisterInfo(String name, String last_name, String email, String cel,
                            String password, String conpassword) {
            this.name = name;
            this.last_name = last_name;
            this.email = email;
            this.cel = cel;
            this.password = password;
            this.conpassword = conpassword;
        }

        public String getName() {
            return name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getEmail() {
            return email;
        }

        public String getCel() {
            return cel;
        }

        public String getPassword() {
            return password;
        }

        public String getConpassword() {
            return conpassword;
        }
    }
}

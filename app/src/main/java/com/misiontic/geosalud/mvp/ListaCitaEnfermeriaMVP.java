package com.misiontic.geosalud.mvp;

import android.app.Activity;

import java.util.Date;
import java.util.List;

public interface ListaCitaEnfermeriaMVP {

    interface  Model{
        void loadCitas(loadCitasCallBack callBack);

        interface  loadCitasCallBack{
            void setCitas(List<CitasListdto> citas);

        }

    }

    interface  Presenter {
        void loadCitas();

    }

    interface  View{
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showCitas(List<CitasListdto> listCitas);
    }

    class CitasListdto{

        private String ClientName;
        private String CitaDate;
        private String Sercicio;


        public CitasListdto(String clientName, String citaDate, String sercicio) {
            this.ClientName = clientName;
            this.CitaDate = citaDate;
            this.Sercicio = sercicio;
        }

        public String getClientName() {
            return ClientName;
        }

        public String getCitaDate() {
            return CitaDate;
        }

        public String getSercicio() {
            return Sercicio;
        }
    }
}

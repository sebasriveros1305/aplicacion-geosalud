package com.misiontic.geosalud.model;

import android.content.Context;
import android.util.Log;

import com.misiontic.geosalud.model.database.entity.User;
import com.misiontic.geosalud.model.database.repository.FirebaseAuthRepository;
import com.misiontic.geosalud.model.database.repository.UserRepository;
import com.misiontic.geosalud.mvp.RegisterMVP;

import java.util.HashMap;
import java.util.Map;

public class RegisterInteractor implements RegisterMVP.Model {

    private  Map<String,String> users;
    private final UserRepository userRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;

    public RegisterInteractor(Context context) {
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance();
        users = new HashMap<>();
        users.put("yo@mail.com", "123456789");
        users.put("yo2@mail.es", "123456789");
    }
    @Override
    public void ValidateCredentials(User user, ValidateCredentialsCallback callback) {

        firebaseAuthRepository.createUser(user, new FirebaseAuthRepository.FirebaseAuthCallback() {

            @Override
            public void onSuccess() {
                user.setUid(firebaseAuthRepository.getCurrentUser().getUid());
                userRepository.createUser(user, new UserRepository.userCallBack<Void>() {
                    @Override
                    public void onSucess(Void node) {
                        Log.i(RegisterInteractor.class.getSimpleName(),"pruba datos"+
                                firebaseAuthRepository.getCurrentUser().getProviderId());
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                         callback.onFailure();
                    }
                });
                callback.onSuccess();
            }

            @Override
            public void onFailure() {
                 callback.onFailure();
            }
        });


       /* if( users.get(user.getEmail()) != null){
            callback.onSuccess();
        }else{
            callback.onFailure();
        }*/

    }
}

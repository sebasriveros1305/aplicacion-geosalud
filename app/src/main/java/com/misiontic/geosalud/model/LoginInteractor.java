package com.misiontic.geosalud.model;


import android.content.Context;

import com.misiontic.geosalud.model.database.entity.User;
import com.misiontic.geosalud.model.database.repository.FirebaseAuthRepository;
import com.misiontic.geosalud.model.database.repository.UserRepository;
import com.misiontic.geosalud.mvp.LoginMVP;

import java.util.ArrayList;
import java.util.List;

public class LoginInteractor implements LoginMVP.Model {

    private final UserRepository userRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;




    public LoginInteractor(Context context) {

        userRepository= new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance();
    }


    @Override
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

        firebaseAuthRepository.singInUser(email,password, new FirebaseAuthRepository.FirebaseAuthCallback() {
            @Override
            public void onSuccess() {
                callback.onSuccess();
            }

            @Override
            public void onFailure() {
               callback.onFailure();
            }
        });

    }
   /* public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

            userRepository.getUserByEmail(email, new UserRepository.userCallBack<User>() {
                @Override
                public void onSucess(User user) {
                    if (user != null && user.getPassword().equals(password)){
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }

                }
                @Override
                public void onFailure() {
                    callback.onFailure();

                }
            });
    }*/

    @Override // puedo colocar filtros 22/11->2:25
    public void getAllUsers(getUserCallBack<List<LoginMVP.UserInfo>> callBack) {
        userRepository.getAllUser(new UserRepository.userCallBack<List<User>>() {
            @Override
            public void onSucess(List<User> values) {
                List<LoginMVP.UserInfo> users= new ArrayList<>();
                for (User user:values){
                    users.add(new LoginMVP.UserInfo(user.getEmail(),user.getPassword(), user.getTelNum(), user.getName()));
                }
                callBack.onSuccess(users);

            }

            @Override
            public void onFailure() {
                callBack.onFailure();
            }
        });
    }

}
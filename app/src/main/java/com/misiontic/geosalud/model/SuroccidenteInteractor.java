package com.misiontic.geosalud.model;

import com.misiontic.geosalud.mvp.SuroccidenteMVP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SuroccidenteInteractor implements SuroccidenteMVP.Model {

    private final List<SuroccidenteMVP.SuroccidenteDto> hospital;

    public SuroccidenteInteractor(){
        hospital = Arrays.asList(
                new SuroccidenteMVP.SuroccidenteDto("Hospital Fontibon E.S.E", "Cra 99 Clle 16 i", "14860033, Bogota, Colombia"),
                new SuroccidenteMVP.SuroccidenteDto("Dispensario Medico Sur Occidente", "Ak. 86 ##53 b sur", "17944222, Bogota, Colombia" ),
                new SuroccidenteMVP.SuroccidenteDto("Hospital El Tintal", "Cl. 10 #86-58", "3216099824, Bogota, Colombia" ),
                new SuroccidenteMVP.SuroccidenteDto("Hospital Fontibón UPA 48 San Pablo", "Cl. 18a #12225", "12675284, Bogota, Colombia"),
                new SuroccidenteMVP.SuroccidenteDto("Salud Publica HF", "Cra. 98 #16i-32", "14734567, Bogota, Colombia"),
                new SuroccidenteMVP.SuroccidenteDto("Centro de Salud (UPA) 49", "CL. 23 # 112-60", "148600033, Bogota, Colombia"),
                new SuroccidenteMVP.SuroccidenteDto("Hospital San Rafael", "Cra. 96b &, Cl. 17 ", "148456789, Bogota, Colombia")
        );

    }

    @Override
    public void loadSuroccidente(loadSuroccidenteCallback callback) {

        callback.setSuroccidente(hospital);

    }
}

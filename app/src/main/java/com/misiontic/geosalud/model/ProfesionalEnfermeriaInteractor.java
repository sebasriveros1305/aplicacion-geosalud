package com.misiontic.geosalud.model;

import com.misiontic.geosalud.mvp.ProfesionalEnfermeriaMVP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProfesionalEnfermeriaInteractor implements ProfesionalEnfermeriaMVP.Model {

    private List<ProfesionalEnfermeriaMVP.ProfesionalesListdto> ListProfesionales;

    public ProfesionalEnfermeriaInteractor (){
        ListProfesionales = Arrays.asList(
                new ProfesionalEnfermeriaMVP.ProfesionalesListdto("Maria Mendez","20/12/2021 08:15:00"),
                new ProfesionalEnfermeriaMVP.ProfesionalesListdto("Juan Suarez","26/12/2021 18:15:00")

        );
    }

    @Override
    public void loadProfesionales(loadProfesionalesCallBack callBack){

        try{
            Thread.sleep(5000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        callBack.setProfesionales(ListProfesionales);

    }




}

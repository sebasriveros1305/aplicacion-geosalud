package com.misiontic.geosalud.model;

import com.misiontic.geosalud.mvp.FarmaciaMVP;

import java.util.Arrays;
import java.util.List;

public class FarmaciaInteractor implements FarmaciaMVP.Model {

    private List<FarmaciaMVP.FarmaciaDto> farmacia;
    public FarmaciaInteractor(){
        farmacia = Arrays.asList(
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13"),
                new FarmaciaMVP.FarmaciaDto("Drogueria unidad","car 16 calle 13")
        );
    }
    @Override
    public void loadFarmacia(LoadFarmaciaCallback callback) {
        try {
            Thread.sleep(2000);

        }catch (InterruptedException e){
            e.printStackTrace();
        }
        callback.setFarmacia(farmacia);
    }
}
package com.misiontic.geosalud.model.database.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.misiontic.geosalud.model.database.GeoSaludDatabase;
import com.misiontic.geosalud.model.database.dao.UserDao;

import com.misiontic.geosalud.model.database.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private final UserDao userDao;
    private final DatabaseReference userRef;
    private final Boolean inDb=false;

    public UserRepository(Context context) {

        this.userDao = GeoSaludDatabase.getInstance(context)
            .getUserDao();

        FirebaseDatabase database= FirebaseDatabase.getInstance();
        userRef= database.getReference("user");

        loadInitialUser();

    }


    private void loadInitialUser() {
        if(inDb){
            //Room DB
            userDao.insert(new User("grupo4","grupo4@gmail.com","3114526658","123456789"));
            userDao.insert(new User("grupo04","grupo04@gmail.com","3114526658","987654321"));
        }else{
            //Firebase
            userRef.child("grupo4_gmail_com").child("name").setValue("Grupo4");
            userRef.child("grupo4_gmail_com").child("email").setValue("grupo4@gmail.com");
            userRef.child("grupo4_gmail_com").child("telefono").setValue("3114526658");
            userRef.child("grupo4_gmail_com").child("password").setValue("123456789");

            User user = new User("grupo04","grupo04@gmail.com","3114526658","987654321");
            userRef.child(getEmailId(user.getEmail())).setValue(user);
        }
    }

    //Leer datos de la BD
    public void getUserByEmail(String email, userCallBack<User> callBack) {
        if(inDb) {
            //Room DB
            callBack.onSucess(userDao.getUserEmail(email));
        }else{

            userRef.child(getEmailId(email))
                    .addValueEventListener(new ValueEventListener() {
                        @Override //Enceuntra un valor con ese ID
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            User user= dataSnapshot.getValue(User.class);
                            callBack.onSucess(user);
                        }
                        @Override //Si no lo encontro
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            callBack.onFailure();
                        }
                    });
        }

    }

    //Traer toda la lista de la BD Users
    public void getAllUser (userCallBack <List<User>> callBack){
        if(inDb) {
            //Room DB
            callBack.onSucess(userDao.getAll());
        }else{
            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<User> users= new ArrayList<>();
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        users.add(snapshot.getValue(User.class));
                    }
                    callBack.onSucess(users);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    userRef.removeEventListener(this);
                    callBack.onFailure();
                }
            });
        }

    }

    public  void  createUser(User user ,userCallBack<Void> callback){

        if(inDb){
            callback.onSucess(null);
        }else{
            User userSave = new User(user.getName(),user.getEmail(),user.getTelNum(),null);
            userSave.setEnable(userSave.getEnable());
            userRef.child(user.getUid()).setValue(userSave);
            callback.onSucess(null);
        }

    }

    private  String getEmailId(String email){
        return email.replace('@','_').replace('.','_');

    }

    //CallBack para uso de la BD
    public static interface getUserByEmailCallBack{
        void onSucess(User user);
        void onFailure();
    }

    //CallBack generico
    public static interface userCallBack<T>{
        void onSucess(T node);
        void onFailure();
    }
}

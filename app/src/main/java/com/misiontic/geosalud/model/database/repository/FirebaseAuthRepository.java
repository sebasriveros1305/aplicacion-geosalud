package com.misiontic.geosalud.model.database.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.misiontic.geosalud.model.database.entity.User;

import java.util.concurrent.Executor;

public class FirebaseAuthRepository {

    private static  FirebaseAuthRepository instance;


    public static FirebaseAuthRepository getInstance(){

        if(instance == null){
            instance = new FirebaseAuthRepository();
        }
        return instance;
    }

    private FirebaseAuth mAuth;
    private final DatabaseReference userRef;
    private FirebaseUser currentUser;
    private FirebaseAuthRepository(){

        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database= FirebaseDatabase.getInstance();
        userRef= database.getReference("user");


    }
    public boolean isAuthenticate(){

        return getCurrentUser() != null;

    }

    public FirebaseUser getCurrentUser(){
        if (currentUser == null){
            currentUser = mAuth.getCurrentUser();
        }
        return  currentUser;
    }
    public void createUser(User user,FirebaseAuthCallback callback){

        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        //TODO creo el usuario correctamnete
                        currentUser= mAuth.getCurrentUser();
                        callback.onSuccess();
                      //  userRef.child(user.getEmail()).setValue(user);
                        Log.i(FirebaseAuthRepository.class.getSimpleName(),"usuario  creado");

                    } else {
                        callback.onFailure();
                        Log.i(FirebaseAuthRepository.class.getSimpleName(),"usuario no creado");
                    }
                });
    }

    public  void singInUser(String email, String password, FirebaseAuthCallback callback){

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.i(FirebaseAuthRepository.class.getSimpleName(), "signInWithEmail:success");
                            currentUser = mAuth.getCurrentUser();
                            callback.onSuccess();
                        } else {
                            // If sign in fails, display a message to the user.
                            callback.onFailure();
                            Log.i(FirebaseAuthRepository.class.getSimpleName(),"usuario no creado");
                        }

                });

    }

    public interface  FirebaseAuthCallback{

        void onSuccess();

        void onFailure();
    }
}

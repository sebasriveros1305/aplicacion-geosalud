package com.misiontic.geosalud.model;

import com.misiontic.geosalud.mvp.CitaEnfermeriaMVP;
import com.misiontic.geosalud.mvp.ListaCitaEnfermeriaMVP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListaCitasEnfermeriaInteractor implements ListaCitaEnfermeriaMVP.Model {

    private List<ListaCitaEnfermeriaMVP.CitasListdto> ListCitas;

    public ListaCitasEnfermeriaInteractor(){
        ListCitas = Arrays.asList(
                new ListaCitaEnfermeriaMVP.CitasListdto("Paola Calderon","20/11/2021 08:15:00","Acompañamiento"),
                new ListaCitaEnfermeriaMVP.CitasListdto("Lina Ortegon","18/11/2021 08:15:00","Acompañamiento"),
                new ListaCitaEnfermeriaMVP.CitasListdto("Luis Diaz","30/12/2021 08:15:00","Acompañamiento"),
                new ListaCitaEnfermeriaMVP.CitasListdto("Pepito ","01/01/2022 08:15:00","Acompañamiento")
        );

    }


    @Override
    public void loadCitas(loadCitasCallBack callBack) {

        try{
            Thread.sleep(5000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        callBack.setCitas(ListCitas);



    }
}

package com.misiontic.geosalud.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.misiontic.geosalud.model.database.dao.UserDao;
import com.misiontic.geosalud.model.database.entity.User;

@Database(entities={User.class}, version=1)
public abstract class GeoSaludDatabase extends RoomDatabase {

    private volatile static GeoSaludDatabase instance;

    public static  GeoSaludDatabase getInstance(Context context){

        if(instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(),GeoSaludDatabase.class,"geosalud-database")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    };


    public abstract UserDao getUserDao();
}

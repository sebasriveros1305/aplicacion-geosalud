package com.misiontic.geosalud.presenter;

import com.misiontic.geosalud.model.ProfesionalEnfermeriaInteractor;
import com.misiontic.geosalud.mvp.ProfesionalEnfermeriaMVP;

import java.util.List;

public class ProfesionalEnfermeriaPresenter implements ProfesionalEnfermeriaMVP.presenter {

    private ProfesionalEnfermeriaMVP.View view;
    private ProfesionalEnfermeriaMVP.Model model;

    public ProfesionalEnfermeriaPresenter(ProfesionalEnfermeriaMVP.View view){
        this.view = view;
        this.model= new ProfesionalEnfermeriaInteractor();
    }


    @Override
    public void loadProfesionales() {
        view.showProgressBar();

        new Thread(()->{
            model.loadProfesionales(new ProfesionalEnfermeriaMVP.Model.loadProfesionalesCallBack() {
                @Override
                public void setProfesionales(List<ProfesionalEnfermeriaMVP.ProfesionalesListdto> ListProfesionales) {
                    view.getActivity().runOnUiThread(()->{
                        view.hideProgressBar();
                        view.showProfesionales(ListProfesionales);
                    });
                }
            });
        }).start();

    }
}

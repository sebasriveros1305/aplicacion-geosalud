package com.misiontic.geosalud.presenter;

import android.os.Bundle;

import com.misiontic.geosalud.model.SuroccidenteInteractor;
import com.misiontic.geosalud.mvp.SuroccidenteMVP;

import java.util.List;

public class SuroccidentePresenter implements SuroccidenteMVP.Presenter {

    private SuroccidenteMVP.View view;
    private SuroccidenteMVP.Model model;

    public SuroccidentePresenter(SuroccidenteMVP.View view) {
        this.view = view;
        this.model = new SuroccidenteInteractor();
    }

    @Override
    public void loadSuroccidente() {
    model.loadSuroccidente(new SuroccidenteMVP.Model.loadSuroccidenteCallback() {
    @Override
    public void setSuroccidente(List<SuroccidenteMVP.SuroccidenteDto> hospital) {
        view.showSuroccidente(hospital);

    }
});
    }

    @Override
    public void onSuroccidenteClick() {

    }

    @Override
    public void onSelectItem(SuroccidenteMVP.SuroccidenteDto item) {
        Bundle params = new Bundle();
        params.putString("hospital", item.getHospital());
        params.putString("address", item.getAddress());
        params.putString("telephone", item.getTelephone());

        view.openLocationActivity(params);
    }
    @Override
    public void onNewHospitalClick() {

    }
}

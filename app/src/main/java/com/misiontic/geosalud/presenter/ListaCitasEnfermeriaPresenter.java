package com.misiontic.geosalud.presenter;

import com.misiontic.geosalud.model.ListaCitasEnfermeriaInteractor;
import com.misiontic.geosalud.mvp.ListaCitaEnfermeriaMVP;

import java.util.List;

public class ListaCitasEnfermeriaPresenter implements ListaCitaEnfermeriaMVP.Presenter {

    private ListaCitaEnfermeriaMVP.View view;
    private  ListaCitaEnfermeriaMVP.Model model;

    public ListaCitasEnfermeriaPresenter(ListaCitaEnfermeriaMVP.View view) {
        this.view = view;
        this.model = new ListaCitasEnfermeriaInteractor();
    }

    @Override
    public void loadCitas() {
        view.showProgressBar();

        new Thread(() ->{
            model.loadCitas(new ListaCitaEnfermeriaMVP.Model.loadCitasCallBack() {

                @Override
                public void setCitas(List<ListaCitaEnfermeriaMVP.CitasListdto> ListCitas ) {
                    view.getActivity().runOnUiThread(() -> {
                        view.hideProgressBar();
                        view.showCitas(ListCitas);
                    });
                }
            });
        }).start();


    }
}

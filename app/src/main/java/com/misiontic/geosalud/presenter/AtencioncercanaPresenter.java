package com.misiontic.geosalud.presenter;

import com.misiontic.geosalud.model.AtencioncercanaInteractor;
import com.misiontic.geosalud.mvp.AtencioncercanaMVP;

public class AtencioncercanaPresenter implements AtencioncercanaMVP.Presenter {

    private AtencioncercanaMVP.View view;
    private AtencioncercanaMVP.Model model;

    public AtencioncercanaPresenter(AtencioncercanaMVP.View view) {
        this.view = view;
        this.model = new AtencioncercanaInteractor();
    }

    @Override
    public void loadAtencioncercana() {

    }

    @Override
    public void onNewRegionClick() {

    }


}

package com.misiontic.geosalud.presenter;

import com.misiontic.geosalud.mvp.MenuEnfermeriaMVP;

public class MenuEnfermeriaPresenter implements MenuEnfermeriaMVP.Presenter {

    private MenuEnfermeriaMVP.View view;
    private MenuEnfermeriaMVP.Model model;

    public MenuEnfermeriaPresenter(MenuEnfermeriaMVP.View view){
        this.view=view;
        this.model=null;

    }


    @Override
    public void onAtencionCasaClick() {
        view.showAgendaTuCita();

    }

    @Override
    public void onAcompanamimentoClick() {
        view.showAgendaTuCita();

    }

    @Override
    public void onConsultaCitaClick() {
        view.showListaCitasEnfermeria();

    }
}

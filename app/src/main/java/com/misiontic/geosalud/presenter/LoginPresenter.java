package com.misiontic.geosalud.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.misiontic.geosalud.model.LoginInteractor;
import com.misiontic.geosalud.mvp.LoginMVP;

import java.util.List;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void onLoginClick() {
        boolean error = false;
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
        //Validacion de datos
        view.showEmailError("");
        view.showPasswordError("");
        if (loginInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico es obligatorio");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (loginInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad");
            error = true;
        }

        if (!error) {
            view.showProgresBar();
            new Thread(() ->model.validateCredentials(loginInfo.getEmail(), loginInfo.getPassword(),
                    new LoginMVP.Model.ValidateCredentialsCallback() {
                        @Override
                        public void onSuccess() {
                            SharedPreferences preferences = view.getActivity()
                                    .getSharedPreferences("auth", Context.MODE_PRIVATE);
                            preferences.edit()
                                    .putBoolean("isLogin",true)
                            .apply();

                            view.getActivity().runOnUiThread(() -> {
                                view.showMenuActivity();
                                view.hideProgresBar();
                            });
                        }

                        @Override
                        public void onFailure() {
                            view.getActivity().runOnUiThread(() -> {
                                view.ShowgenerarError("Credenciales inválidas");
                                view.hideProgresBar();
                            });
                        }
                    })).start();

            new Thread(() ->model.getAllUsers(new LoginMVP.Model.getUserCallBack<List<LoginMVP.UserInfo>>() {

                @Override
                public void onSuccess(List<LoginMVP.UserInfo> data) {
                    for (LoginMVP.UserInfo user: data) {
                        Log.i(LoginPresenter.class.getSimpleName(), user.toString());
                    }

                }

                @Override
                public void onFailure() {
                    Log.e(LoginPresenter.class.getSimpleName(),"No hay datos en BD");

                }
            })).start();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") &&
                email.endsWith(".com") ||
                email.endsWith(".co") ||
                email.endsWith(".es");

    }
    @Override
    public void onFacebookClick () {

    }

    @Override
    public void onTwitterClick () {

    }

    @Override
    public void onInscribirseClick () {
        view.showRegister();

    }

    @Override
    public void onOlvidosucontrasenaClick () {

    }

    @Override
    public void isLoggedUser() {
        SharedPreferences preferences = view.getActivity()
                .getSharedPreferences("auth", Context.MODE_PRIVATE);

        boolean isLogged= preferences.getBoolean("isLogin",false);

        if(isLogged){
            view.showMenuActivity();
        }

    }


}
package com.misiontic.geosalud.presenter;

import com.misiontic.geosalud.model.RegisterInteractor;
import com.misiontic.geosalud.model.database.entity.User;
import com.misiontic.geosalud.mvp.RegisterMVP;

public class RegisterPresenter implements RegisterMVP.Presenter {

    private RegisterMVP.View view;
    private RegisterMVP.Model model;

    public RegisterPresenter(RegisterMVP.View view) {

        this.view = view;
        this.model = new RegisterInteractor(view.getActivity());
    }

    @Override
    public void onRegisterclick() {
        boolean error = false;

        RegisterMVP.RegisterInfo registerInfo = view.getRegisterInfo();
        Limpiarvista();

        if(registerInfo.getName().isEmpty()){
            view.showNameError("nombre Obligatorio");
            error = true;
        }
        if(registerInfo.getLast_name().isEmpty()){
            view.showLastnameError("Apellido Obligatorio");
            error = true;
        }
        if(registerInfo.getEmail().isEmpty()){
            view.showEmailError("Corre Obligatorio");
            error = true;
        }else if(!isEmailValid(registerInfo.getEmail())){
            view.showEmailError("Corre No valido ");
            error = true;
        }

        if(registerInfo.getCel().isEmpty()){
            view.showCelError("Telefono  Obligatorio");
            error = true;
        }

        if(registerInfo.getPassword().isEmpty()){
            view.showPasswordError("Contraseña Obligatoria");
            error = true;
        }else if(!isPasswordlValid(registerInfo.getPassword())){
            view.showPasswordError("Contraseña debe tener minimo 8 caracteres entre numero y letras");
            error = true;
        }

        if(registerInfo.getConpassword().isEmpty()){
            view.showConPasswordError("Campo Obligatorio");
            error = true;
        }else if(!isConPasswordlValid(registerInfo.getConpassword())){
            view.showConPasswordError("Contraseña debe tener minimo 8 caracteres entre numero y letras");
            error = true;
        }
        if (!registerInfo.getPassword().equals(registerInfo.getConpassword())) {
            view.showPasswordGeneralError("Contraseñas no son iguales");
            error = true;
        }

        if(!error) {
            view.showProgress();
            User user = new User(registerInfo.getName()+" "+registerInfo.getLast_name(), registerInfo.getEmail(),
                                registerInfo.getCel(), registerInfo.getConpassword());
            new Thread(()->
                    model.ValidateCredentials(user,new RegisterMVP.Model.ValidateCredentialsCallback(){

                        @Override
                        public void onSuccess() {
                            view.getActivity().runOnUiThread(()->{
                                view.hideProgress();
                                view.showMenuActivity();
                                view.showGeneralError("Usuario creado");

                            });

                        }

                        @Override
                        public void onFailure() {
                            view.getActivity().runOnUiThread(()->{
                                view.hideProgress();
                                view.showGeneralError("Correo en uso");
                            });

                        }
                    })).start();

        }




    }

    private boolean isConPasswordlValid(String conpassword) {

        return  conpassword.length() >=8 &&
                veriFicarCaracter(conpassword) &&
                VeriFicarNum(conpassword);
    }

    private boolean VeriFicarNum(String password) {
        for (int i = 1; i < password.length(); i++) {
            if (password.charAt(i) >= 'a' && password.charAt(i) <= 'z' ||
                    password.charAt(i) >= 'A' && password.charAt(i) <= 'Z'){
                return true;
            }
        }
        return false;
    }

    private boolean veriFicarCaracter(String password) {
        for (int i = 1; i < password.length(); i++)
            if (password.charAt(i) >= '0' && password.charAt(i) <= '9') return true;
        return false;
    }



    private boolean isPasswordlValid(String password) {
        return  password.length() >=8 &&
                veriFicarCaracter(password)&&
                VeriFicarNum(password);
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") &&
                email.endsWith(".com") ||
                email.endsWith(".co") ||
                email.endsWith(".es");
    }

    private void Limpiarvista() {
        view.showNameError("");
        view.showLastnameError("");
        view.showEmailError("");
        view.showCelError("");
        view.showPasswordError("");
        view.showConPasswordError("");

    }
}

package com.misiontic.geosalud.presenter;

import com.misiontic.geosalud.model.CitaEnfermeriaInteractor;
import com.misiontic.geosalud.mvp.CitaEnfermeriaMVP;

public class CitaEnfermeriaPresenter implements CitaEnfermeriaMVP.Presenter {

    private CitaEnfermeriaMVP.View view;
    private CitaEnfermeriaMVP.Model model;

    public CitaEnfermeriaPresenter(CitaEnfermeriaMVP.View view){
        this.view=view;
        this.model= null;

    }



    @Override
    public void onSaveClick() {
        CitaEnfermeriaMVP.CalendarInfo calendarInfoview = view.getCalendarInfo();
        view.showCalendarError("");

        if(calendarInfoview.getCalendarDate().isEmpty()){
            view.showCalendarError("Seleciona una fecha para tu cita");
        }else{
            view.showCalendarError("");
            view.showProfesionalSalud();
        }




    }
}

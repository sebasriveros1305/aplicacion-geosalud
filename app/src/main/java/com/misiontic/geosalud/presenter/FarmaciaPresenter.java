package com.misiontic.geosalud.presenter;

import android.os.Bundle;

import com.misiontic.geosalud.model.FarmaciaInteractor;
import com.misiontic.geosalud.mvp.FarmaciaMVP;

import java.util.List;

public class FarmaciaPresenter implements FarmaciaMVP.Presenter {
    private FarmaciaMVP.View view;
    private FarmaciaMVP.Model model;

    public FarmaciaPresenter(FarmaciaMVP.View view) {
        this.view = view;
        this.model = new FarmaciaInteractor();
    }

    @Override
    public void LoadFarmacia() {
        view.showProgresssBar();
        new Thread(()->{
            model.loadFarmacia(new FarmaciaMVP.Model.LoadFarmaciaCallback() {
                @Override
                public void setFarmacia(List<FarmaciaMVP.FarmaciaDto> Farmacia) {
                    view.getAtivity().runOnUiThread(()->{
                        view.hideProgressBar();
                        view.showFarmacia(Farmacia);
                    });
                }
            });
        }).start();

    }

    @Override
    public void onClikCancel() {
        view.showManuActivity();
    }

    @Override
    public void OnSelectItem(FarmaciaMVP.FarmaciaDto item) {

        Bundle params = new Bundle();
        params.putString("name",item.getName());
        params.putString("address",item.getAddress());
        view.openDetailActivity(params);
    }


}
